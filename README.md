# log4rs-sentry [![pipeline status](https://gitlab.com/msrd0/log4rs-sentry/badges/master/pipeline.svg)](https://gitlab.com/msrd0/log4rs-sentry/commits/master) [![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) [![dependency status](https://deps.rs/repo/gitlab/msrd0/log4rs-sentry/status.svg)](https://deps.rs/repo/gitlab/msrd0/log4rs-sentry)

Sentry Appender for log4rs

## Usage

Add the following to your `Cargo.toml`:

```toml
log4rs-sentry = "^1.0"
```

Now, when configuring your `log4rs` logger, use the following code:

```rust
use log4rs_sentry::SentryAppender;

let sentry_encoder = PatternEncoder::new("{m}");
let sentry = SentryAppender::new(Box::new(sentry_encoder));

let config = Config::builder()
		.appender(Appender::builder().build("sentry", Box::new(sentry)))
		// other appenders
		.build(Root::builder()
				.appender("sentry")
				// other appenders
				.build(LevelFilter::Warn))
		.unwrap();
```
